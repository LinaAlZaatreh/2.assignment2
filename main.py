import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.datasets import fetch_openml
from sklearn.metrics import silhouette_score, adjusted_rand_score

mnist = fetch_openml('mnist_784')
images = mnist.data.to_numpy()
labels = mnist.target

images = images / 255.0

new_size = (14, 14)
resized_images = np.array([np.resize(image, new_size).flatten() for image in images])

pca = PCA(n_components=2)
images_pca = pca.fit_transform(resized_images)

k = 10
kmeans = KMeans(n_clusters=k, random_state=42, init='random', n_init=10)
kmeans.fit(images_pca)

cluster_labels = kmeans.labels_
cluster_centers = kmeans.cluster_centers_

silhouette_avg = silhouette_score(images_pca, cluster_labels)
adjusted_rand_idx = adjusted_rand_score(labels, cluster_labels)

plt.scatter(images_pca[:, 0], images_pca[:, 1], c=cluster_labels, cmap='viridis')
plt.scatter(cluster_centers[:, 0], cluster_centers[:, 1], c='red', marker='x')
plt.title("K-means Clustering on MNIST")
plt.show()

print(f"Silhouette Score: {silhouette_avg}")
print(f"Adjusted Rand Index: {adjusted_rand_idx}")
